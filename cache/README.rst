Cache
=====
Aquí van las citas. He puesto citas de wikiquotes; en general de:

* Linux Torvalds
* Proverbios Beltas (por que están bien Belgas!)
* Richard Stallman

Había hecho un scrapper para Wikiquotes usando la API y xPath pero no jalaba bien (no le agarré bien el rollo al xPath).

Aquí el ejemplo:

.. code-block:: crystal

    require "json"
    require "http/client"
    require "xml"

    src = "https://es.wikiquote.org/w/api.php?format=json&action=parse&page=Proverbios_belgas"

    body = HTTP::Client.get( src ).body
    content = JSON.parse( body )["parse"]["text"]["*"].to_s
    nodes = XML.parse_html(content)

    nodes.xpath_nodes("//ul/li").each do |node|
      puts node.text.gsub(/^.*«/, "").gsub(/».*$/, "") + '.'
    end

Si alguien sabe como arreglarlo y quiere implementar redis o algo para cuando fallen, adelante. A mí, por hoy, me dió weva.
