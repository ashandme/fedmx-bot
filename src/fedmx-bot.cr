require "tourmaline"
require "schedule"

# TODO: Write documentation for `Fedmx::Bot`
module Fedmx::Bot
  VERSION = "0.1.0"

  class Post < Tourmaline::Client
    ABC = {
      'a' => '4',
      'b' => '8',
      'c' => 'c',
      'd' => '6',
      'e' => '3',
      'f' => 'f',
      'g' => '9',
      'h' => "|-|",
      'i' => '¡',
      'j' => 'j',
      'k' => "|<",
      'l' => '1',
      'm' => "nn",
      'o' => '0',
      'p' => "|0",
      'q' => '9',
      'r' => '7',
      's' => '5',
      't' => '+',
      'u' => "|_|",
      'v' => "\/",
      'w' => "\/\/",
      'x' => 'X',
      'y' => 'y',
      'z' => '2'
    }

    CHANNEL_ID = -435388555

    @[Command("echo")]
    def echo_command(ctx)
      message = ctx.text
      message = message.downcase
      message = message.gsub(ABC)

      ctx.message.reply(message)
    end

    def post_to_channel
      quote = File.read_lines("cache/quotes.txt").shuffle.pop
      quote = quote.downcase
      quote = quote.gsub(ABC)

      send_message(CHANNEL_ID, quote)
    end
  end

  bot = Post.new(bot_token: ENV["API_KEY"])

  Schedule.every(:day, "13:37:00") do
    bot.post_to_channel
  end

  bot.poll
end
