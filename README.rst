fedmx-bot
=========

Un bot de juguete; escrito con Crystal. Lo único que hace es dar un mensaje del día en 1337.

Instalación
-----------
Nomás agrega los shards. Por lo pronto, hay que hacerlo de manera en que los mismos ignoren la versión de Crystal:

.. code-block:: sh

    shards install --ignore-crystal-version

Uso
---
Para prenderlo, tienes que exportar una variable de entorno llamada ``API_KEY``. Ya que la exportes, solo córrelo:

.. code-block:: sh

    crystal run src/fedmx-bot

Contribuyendo
-------------
#. Bifúrcalo (<https://gitlab.com/renich/fedmx-bot/fork>)
#. Crea tu rama de funcionalidad (``git checkout -b my-funcionalidad``)
#. Guarda tus cambios (``git commit -am 'Agregué esta funcionalidad'``)
#. Empuja a tu rama (``git push --set-upstream origin mi-nueva-funcionalidad``)
#. Crea una petición de jalado.

Contribuyentes
==============
* Renich Bon Ciric - creador y mantenedor.
